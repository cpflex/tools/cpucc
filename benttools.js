#! /usr/bin/env node
/**
 *  Name:  tools.h
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
const util = require("util");
var fs = require("fs");
const yaml = require("js-yaml");
const { DEFAULT_SCHEMA } = require("js-yaml");
const path = require("path");
const csv = require("convert-array-to-csv");
const bent = require("bent");

const sUrlDefault = "http://global.cpflex.tech/integration/v1.0";

/*
 * * Recursively merge properties of two objects into object 1.
 * */
function _mergeObjects(obj1, obj2) {
  for (let p in obj2) {
    try {
      // Property in destination object set; update its value.
      if (obj2[p].constructor == Object) {
        obj1[p] = _mergeObjects(obj1[p], obj2[p]);
      } else {
        obj1[p] = obj2[p];
      }
    } catch (e) {
      // Property in destination object not set; create it and set its value.
      obj1[p] = obj2[p];
    }
  }
  return obj1;
}

function _addCommonOptions(program, bRequireAccKey = false) {
  if (bRequireAccKey) {
    program.option(
      "-A, --acckey <acckey>",
      "CP-Flex secondary API access key. Some secured APIs require a valid access key granting access to the specific system resource. Can also be specified in the CPUCC_ACCKEY environment variable."
    );
  }

  program.option(
    "-U, --url <url>",
    'Overrides default or "CPUCC_URL" environment variable.',
    sUrlDefault
  );
  program.option("-J, --json", "formats output as JSON.", false);
  program.option("-Y, --yaml", "formats output as YAML.", false);
  program.option("-C, --csv", "Converts tabular array to CSV format.", false);
  program.option(
    "-T --table",
    "format output as a table.  Good for lists",
    false
  );
  program.option(
    "-F --file <filename>",
    "Writes output to the specified file."
  );
  program.option(
    "-K, --apikey <apikey>",
    'CP-Flex account API Key must be defined or specified in environment variable "CPUCC_APIKEY" '
  );
  program.option("-v, --verbose", "Verbose output.", false);
}

function _stripQuotes(str) {
  if (str == null || str.length == 0) return null;
  else return str.replace(/^"|"$/g, "");
}

function _getUrl(program) {
  if (program.url == sUrlDefault && !(process.env.CPUCC_URL === undefined)) {
    return _stripQuotes(process.env.CPUCC_URL);
  } else return program.url;
}

function _getApiKey(program) {
  if (program.apikey === undefined) {
    if (
      process.env.CPUCC_APIKEY === undefined ||
      process.env.CPUCC_APIKEY.length == 0
    )
      return null;
    else return _stripQuotes(process.env.CPUCC_APIKEY);
  } else return program.apikey;
}

function _logHeader(program) {
  if (program.verbose === true) {
    console.log("");
    console.log("URL:    " + _getUrl(program));
    console.log("APIKEY: " + _getApiKey(program));
  }
}

function _getCommonHeader(program) {
  let keyval = _getApiKey(program);
  if (keyval == null)
    throw new Error(
      '"--apikey" option and CPUCC_APIKEY environment variable are not defined.  Cannot call service without API key.'
    );

  var idx = keyval.indexOf("acctid|");
  if (idx >= 0) {
    keyval = keyval.substr(idx + 7);
    keytag = "x-consumer-custom-id";
  } else keytag = "apikey";

  var headers;

  if (keyval == "ignore") {
    headers = {
      "Content-Type": "application/json",
      Accept: "application/json",
    };
  } else {
    headers = {
      "Content-Type": "application/json",
      Accept: "application/json",
      [keytag]: keyval,
    };
  }
  return headers;
}

/**
 * Returns a post object for executing multiple requests on the same API service.
 * @param {*} program
 * @param {*} bRequireAccKey
 * @param {*} statusCode
 * @returns {{post: object, hdr :[string]}} Returns the post and header.
 */
function _createPostClient(program, bRequireAccKey = false, statusCode) {
  if (bRequireAccKey && !body.hasOwnProperty("acckey")) {
    if (program.acckey != undefined) {
      body.acckey = program.acckey;
    } else if (
      process.env.CPUCC_ACCKEY != undefined &&
      process.env.CPUCC_ACCKEY.length > 0
    ) {
      body.acckey = _stripQuotes(process.env.CPUCC_ACCKEY);
    } else {
      throw new Error(
        "'--acckey' and 'CPUCC_ACCKEY' environment variable are not defined. This API requires an access key."
      );
    }
  }

  _logHeader(program);
  let post = bent(_getUrl(program), "POST", "json", statusCode);

  return {
    post: post,
    hdr: _getCommonHeader(program),
  };
}

/**
 * Posts
 * @param {*} program
 * @param {*} path
 * @param {*} body
 * @param {*} bRequireAccKey
 * @param {*} statusCode
 * @returns {Promise<results>}
 */
function _postApiRequest(
  program,
  path,
  body,
  bRequireAccKey = false,
  statusCode
) {
  if (bRequireAccKey && !body.hasOwnProperty("acckey")) {
    if (program.acckey != undefined) {
      body.acckey = program.acckey;
    } else if (
      process.env.CPUCC_ACCKEY != undefined &&
      process.env.CPUCC_ACCKEY.length > 0
    ) {
      body.acckey = _stripQuotes(process.env.CPUCC_ACCKEY);
    } else {
      throw new Error(
        "'--acckey' and 'CPUCC_ACCKEY' environment variable are not defined. This API requires an access key."
      );
    }
  }

  _logHeader(program);

  let post = bent(_getUrl(program), "POST", "json", statusCode);
  return post(path, body, _getCommonHeader(program));
}

function _parseResponseBody(body) {
  if (body.length === 0) return body;
  else return JSON.parse(body);
}

/**
 * Publishes the output to file or console in the user specified format.
 * @param {Commander.program} program
 * @param {object} output Output information to publish.
 */
function publishOutput(program, output) {
  var soutput;

  if (program.table) {
    console.table(output);
  } else {
    if (program.yaml) {
      soutput = yaml.dump(output, {
        schema: DEFAULT_SCHEMA,
      });
    } else if (program.json === true) {
      soutput = JSON.stringify(output, null, 2);
    } else if (program.csv === true) {
      soutput = csv.convertArrayToCSV(output);
    } else {
      soutput = util.inspect(output, false, null, true);
    }

    if (program.file != null) {
      if (program.verbose) {
        console.info("Saving output: " + program.file);
      }
      fs.writeFileSync(program.file, soutput);
    } else {
      console.info(soutput);
    }
  }
}

/**
 * Reads specification file either in JSON or YAML formats.
 * @param {string} fname  Filename for the specification to load.
 * @returns {Object} Returns specification converted to object.
 */
function loadSpecification(fname) {
  var obj = null;
  const ext = path.extname(fname).toLowerCase();
  switch (ext) {
    case ".yaml":
    case ".yml":
      obj = yaml.load(fs.readFileSync(fname, { encoding: "utf-8" }));
      break;
    case ".json":
      obj = JSON.parse(fs.readFileSync(fname, { encoding: "utf-8" }));
      break;
    default:
      throw new Error("Unsupported file format: " + path.extname(fname));
  }
  return obj;
}

module.exports = {
  getApiKey: _getApiKey,
  getUrl: _getUrl,
  logHeader: _logHeader,
  parseResponseBody: _parseResponseBody,
  mergeObjects: _mergeObjects,
  addCommonOptions: _addCommonOptions,
  getCommontHeader: _getCommonHeader,
  createPostClient: _createPostClient,
  postApiRequest: _postApiRequest,
  stripQuotes: _stripQuotes,
  publishOutput: publishOutput,
  loadSpecification: loadSpecification,
};
