#! /usr/bin/env node
/**
 *  Name:  cpucc-gc-downlink.h
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
var request = require("request");
const util = require("util");
var ctools = require("./benttools");
const { program } = require("commander");
require("console.table");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------

  program.description(
    "Encodes and downlinks JSON/YAML formatted message content to specified device using the generic consumer downlink API.  First encodes message content into protobuf packet and then posts downlink.",
    {
      devid: "The registered device identifier.",
      filename:
        "File containing one or more messages to be encoded can be in JSON (.js) or YAML (.yml) file formats.  See http://global.cpflex.tech/console/api-docs/?urls.primaryName=Integration%20API#/Untethered%20Message%20Processing/postUntetheredDownlinkEncode for details on file format.",
    }
  );
  program
    .option("-c, --confirm", "Downlink is a confirmed message.", false)
    .option("-s, --schedule <schedule>", "Where downlink is placed in the queue.", "last")
    .option(
      "-t, --ttl <ttl>",
      "time to live in seconds or hours (xxs or yyh).",
      "48h"
    )
    .arguments("<devid> <filename>")

    //------------------------------------------------------------------------
    // Command Implementation
    //------------------------------------------------------------------------
    .action(async function (devid, filename) {
      if (filename) {
        var messages = ctools.loadSpecification(filename);
        if (!Array.isArray(messages)) {
          messages = [messages];
        }
      }

      if (!devid) {
        throw new Error("device identifier not specified");
      }

      if (!messages || messages.length == 0) {
        throw new Error("no messages specified.");
      }

      const input = {
        devid: devid,
        etid: "cpucc untethered downlink",
        confirm: program.confirm,
        schedule: program.schedule,
        ttl: program.ttl,
        messages: messages,
      };

      let { post, hdr } = ctools.createPostClient(program, false, 200);
      if(program.verbose){
        console.info("Body =");
        const js = JSON.stringify(input, null, 2);
        console.info(js);
      }  
      var resp = await post("/consumer/generic/downlink", input, hdr);
      console.info("\r\n** Response:\r\n");
      ctools.publishOutput(program, resp);
    });

  ctools.addCommonOptions(program);
  program.parse(process.argv);
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
