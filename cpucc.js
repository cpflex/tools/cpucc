#! /usr/bin/env node
/**
 *  Name:  cpucc.h
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

const { program } = require("commander");

program
  .version("1.0.0")
  .command(
    "encode <devid> <endpoint> <payload>",
    "Encodes JSON/YAML formatted message to protobuf format. Returns encoded payload and port number for the device/device-type."
  )
  .command(
    "decode <devid> <port> <payload>",
    "Decodes protobuf formatted message to JSON/YAML format. Returns decoded payload message endpoint, and message type information."
  )
  .command(
    "gc-downlink <devid> <endpoint> <payload>",
    "Encodes and downlinks the JSON/YAML formatted message to the specified device. Requires that the generic-consumer service is defined for the associated account."
  )
  .parse(process.argv);
