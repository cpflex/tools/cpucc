#! /usr/bin/env node
/**
 *  Name:  cpucc-decode.h
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
var request = require("request");
const util = require("util");
var ctools = require("./benttools");
const { program } = require("commander");
require("console.table");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------

  program.description(
    "Decodes protobuff encoded uplink packets JSON/YAML formatted messages. Returns decoded content.",
    {
      devid: "The registered device identifier.",
      filename:
        "File containing one or more messages to be decoded can be in JSON (.js) or YAML (.yml) file formats.  See http://global.cpflex.tech/console/api-docs/?urls.primaryName=Integration%20API#/Untethered%20Message%20Processing/postUntetheredUplinkDecode for details on file format.",
    }
  );
  program
    .option("-i, --incsrc", "include source message data.", false)
    .arguments("<devid> <filename>")

    //------------------------------------------------------------------------
    // Command Implementation
    //------------------------------------------------------------------------
    .action(async function (devid, filename) {
      if (filename) {
        var messages = ctools.loadSpecification(filename);
        if (!Array.isArray(messages)) {
          messages = [messages];
        }
      }

      if (!devid) {
        throw new Error("device identifier not specified");
      }

      if (!messages || messages.length == 0) {
        throw new Error("no messages specified.");
      }

      const input = {
        devid: devid,
        etid: "cpucc untethered encode",
        incsrc: program.incsrc,
        messages: messages,
      };

      let { post, hdr } = ctools.createPostClient(program, false, 200);
      var resp = await post("/untethered/uplink/decode", input, hdr);
      console.info("\r\n** Decoded Response:\r\n");
      ctools.publishOutput(program, resp);
    });

  ctools.addCommonOptions(program);
  program.parse(process.argv);
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
