#! /usr/bin/env node
/**
 *  Name:  cpucc-gc.h
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

const { program } = require("commander");

program
  .version("1.0.0")
  .command(
    "downlink <devid> <endpoint> <payload>",
    "Encodes and downlinks the JSON/YAML formatted message to the specified device. Requires that the generic-consumer service is defined for the associated account."
  )
  .parse(process.argv);
