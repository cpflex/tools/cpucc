# CP-Flex&trade; Untethered Protocol Encoder-Decoder Command Line Tools

Node JS based tools provide a easy-to-use command line interface for interacting
with the CP-Flex untethered integration APIs.  The is the CP-Flex API set for manually processing untethered protocol messages.   
The API provides a means to simple encode custom untethered commands without using the CP-Flex cloud stack downlink integration functions.
Depending on the deployment integration,  the cloud-stack may / may-not be used.

Access to the encoding-decoding APIs requires a valid account API key,  please contact Codepoint 
support if you need an account and key set.

## Installation and Setup

### Install Node.js  

The tools require Node.js (javascript) tools to install your system.   If not 
already installed, please find the package for your 
system an install it. Go to https://nodejs.org/en/download/ and select your OS.
Follow the instructions there.

### Clone and Install CPUCC
The tools here are essentially scripts that can be packaged and deployed 
globally on your system. This provides a command line interface for working with 
CP-Flex claim registrar APIs.   

1. Install Git.  The best way to obtain the tools is to use GIT this can be 
   found at https://git-scm.com/downloads 

1. Clone this project to your local computer. Open a command window or 
   terminal, change to a working directory, where you want to clone the source files 
   (e.g. .../projects/cpflex/) and execute the following comand.  
    ```
    clone https://gitlab.com/cpflex/tools/cpucc.git
    ```

1. Once cloned, change directory into the cpucc project and install the the package locally
    ```
    npm install
    ```
    For developers, the source code is freely available to see how to use the APIs.   

1. To access the tools from anywhere on your machine, package them as a single executable. 
   Install pkg to create a single binary and then use the tool to create executables for 
   Windows, MacOS, or linux.
    ```
    npm install -g pkg
    pkg .
    ```
    This will create cpucc-win.exe, cpucc-linux, and cpucc-macos.

1. Select the package for you OS and copy it to a folder that is on your path in order to 
   execute globally.  For example, copy the *cphub-win.exe* to a tools folder (e.g. `C:/bin`) 
   and rename the file, *cpucc.exe*.

1. Once installed, change your working directory to a project folder and execute the following command:
    ``` 
    > cpucc
    Usage: cpucc [options] [command]

    Options:
        -V, --version                      output the version number
        -h, --help                         display help for command

    Commands:
        registrar                          Claim registration commands
        registrar claim <acct> <claimkey>  Claims an unclaimed device
        registrar list <acct>              Lists claimed devices for the specified account
        registrar info <acct> <claimkey>   Retrieves device information given account and a serial number or claimkey
        help [command]                     display help for command
    ```
    If successful, the main help menu should be shown as above.


1.  Set up you environment variables with your account apikey and access key:  
    * On Windows:<br/>
        ```
        set CPUCC_APIKEY="<YOUR API KEY>"
        ```

    * On Linux or similar systems
        ```
        declare -x CPUCC_APIKEY='<YOUR API KEY>"
        ```
    This environment variable can be made permanent by storing in your system configuration. 
    You can also add these on the command line (the '-K' option) 
    if you are changing API KEYS often or don't want them to persist.   

## Using CPUCC

The command line source provides several examples demonstrating the use of the command line tool.  The source data for these examples can be found in the [examples folder](./examples).

Before testing the examples 

1. Make sure `CPUCC_APIKEY` environment variable is defined in your command line environment with valid CP-Flex Cloud Stack API KEY.
2. Register a Cora untethered device matching the device type for the set of supported messages.  The Cloud stack uses the device registration to load encoding/decoding schemas from the hub. 

### Encode JSON and YAML Untethered Messages
These messages demonstrate encoding downlink messages given JSON and YAML untethered message formats.  The device ID shown below is for a previously registered CS1010 device assigned to an internal Codepoint account.   Substitute with your own device ID.
#### 1. Encode [CS1010 Configuration in JSON format](./examples/encode/cs1010-set-config.json)

    cpucc encode B4607750900001F9 ./examples/encode/cs1010-set-config.json -J

results in JSON (-J option) output

```json
{
  "etid": "cpucc untethered encode",
  "devid": "B4607750900001F9",
  "resultcode": 1,
  "messages": [
    {
      "msgtype": "UntetheredMsg",
      "packet": {
        "port": 19,
        "payload": "EDw="
      }
    }
  ]
}
```

#### 2. Encode [CS1010 Configuration in YAML format](./examples/encode/cs1010-set-config.yaml)

    cpucc encode B4607750900001F9 ./examples/encode/cs1010-set-config.yaml -Y

results in YAML (-Y option) output

```yaml
etid: cpucc untethered encode
devid: B4607750900001F9
resultcode: 1
messages:
  - msgtype: UntetheredMsg
    packet:
      port: 19
      payload: EDw=

```

#### 3. Encode [Temperature Sensor Configuration in JSON format](./examples/encode/temp-sensor-config.json)

    cpucc encode B4607750900001F9 ./examples/encode/temp-sensor-config.json -J

results in JSON (-J option) output

```json
{
  "etid": "cpucc untethered encode",
  "devid": "B4607750900001F9",
  "resultcode": 1,
  "messages": [
    {
      "msgtype": "UntetheredMsg",
      "packet": {
        "port": 52,
        "payload": "CAASCAgAEB4YASAAEggIARAZGAEgABIICAIQAxgBIAAYACACKDwwATgB"
      }
    }
  ]
}
```

#### 4. Encode [Temperature Sensor Configuration in YAML format](./examples/encode/temp-sensor-config.yaml)

    cpucc encode B4607750900001F9 ./examples/encode/temp-sensor-config.yaml -Y

results in YAML (-Y option) output

```yaml
etid: cpucc untethered encode
devid: B4607750900001F9
resultcode: 1
messages:
  - msgtype: UntetheredMsg
    packet:
      port: 52
      payload: CAASCAgAEB4YASAAEggIARAZGAEgABIICAIQAxgBIAAYACACKDwwATgB

```

### Decode Protobuf Packets Untethered Messages
These messages demonstrate decoding uplink messages given JSON and YAML messages providing the protobuf encoded packet payload and port information. The device ID shown below is for a previously registered CS1010 device assigned to an internal Codepoint account.   Substitute with your own device ID.


#### 1. Decode [Temperature Sensor Configuration in JSON format](./examples/decode/temp-sensor-config.json)

    cpucc decode B4607750900001F9 ./examples/decode/temp-sensor-config.json -J

results in JSON (-J option) output

```json
{
  "etid": "cpucc untethered encode",
  "devid": "B4607750900001F9",
  "resultcode": 1,
  "messages": [
    {
      "msgtype": "UntetheredMsg",
      "content": {
        "endpoint": "Temperature.Sensor.GetConfig",
        "datatype": "Temperature.Configuration",
        "payload": {
          "id": 0,
          "triggers": [
            {
              "id": 0,
              "threshold": 30,
              "hysteresis": 1,
              "mode": "ABSGT"
            },
            {
              "id": 1,
              "threshold": 25,
              "hysteresis": 1,
              "mode": "ABSLT"
            },
            {
              "id": 2,
              "threshold": 3,
              "hysteresis": 1,
              "mode": "DELTA"
            }
          ],
          "reportInterval": 0,
          "statsReportInterval": 2,
          "samplingInterval": 60,
          "confirmTrigger": true,
          "confirmStats": true
        }
      }
    }
  ]
}

```

#### 2. Decode [Temperature Sensor Configuration in YAML format](./examples/decode/temp-sensor-config.yaml)

    cpucc decode B4607750900001F9 ./examples/decode/temp-sensor-config.yaml -Y

results in YAML (-Y option) output

```yaml
etid: cpucc untethered encode
devid: B4607750900001F9      
resultcode: 1
messages:
  - msgtype: UntetheredMsg   
    content:
      endpoint: Temperature.Sensor.GetConfig
      datatype: Temperature.Configuration
      payload:
        id: 0
        triggers:
          - id: 0
            threshold: 30
            hysteresis: 1
            mode: ABSGT
          - id: 1
            threshold: 25
            hysteresis: 1
            mode: ABSLT
          - id: 2
            threshold: 3
            hysteresis: 1
            mode: DELTA
        reportInterval: 0
        statsReportInterval: 2
        samplingInterval: 60
        confirmTrigger: true
        confirmStats: true
```

### Downlink Untethered Message Data to a Device
The following demonstrates using the tool to downlink untethered messages to the specified device.  Either content or encoded packet data can be specified.  The CP-Flex cloud stack automatically encodes any messages prior to downlink.  It will report any errors encountered.

    TBD ...


---
*Copyright &copy; 2022 Codepoint Technologies, Inc.*<br/>
*All Rights Reserved*

